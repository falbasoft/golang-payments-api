package main

import (
  "fmt"
  "os"

  "github.com/golang-migrate/migrate/v4"
  _ "github.com/golang-migrate/migrate/v4/database/postgres"
  _ "github.com/golang-migrate/migrate/v4/source/file"
  _ "github.com/lib/pq"
)

func main() {
  dbHost     := os.Getenv("POSTGRES_HOST")
  dbUser     := os.Getenv("POSTGRES_USER")
  dbPassword := os.Getenv("POSTGRES_PASSWORD")
  dbName     := os.Getenv("POSTGRES_DATABASE")

  connection := "postgres://%s:%s@%s/%s?sslmode=disable"
  m, err := migrate.New("file://migrations", fmt.Sprintf(connection, dbUser, dbPassword, dbHost, dbName))

  if err != nil {
    panic(err)
  }

  m.Up()
}
