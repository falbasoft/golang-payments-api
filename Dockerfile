From golang:1.11-alpine as builder
MAINTAINER Cezary Falba <cezary@falba.net>
WORKDIR /

RUN apk add bash ca-certificates git gcc g++ libc-dev
ENV GO111MODULE=on

RUN mkdir -p /tmp/payments
COPY . /tmp/payments/

WORKDIR /tmp/payments/

RUN go mod download
RUN CGO_ENABLED=0 GOOS=linux go build -ldflags="-s -w" -o bin/server cmd/payments-server/main.go
RUN CGO_ENABLED=0 GOOS=linux go build -ldflags="-s -w" -o bin/migrate cmd/migrate/main.go

FROM alpine:latest

RUN adduser -D payments
USER payments

WORKDIR /home/payments/
COPY --from=builder /tmp/payments/bin/server .
COPY --from=builder /tmp/payments/bin/migrate .
COPY --from=builder /tmp/payments/migrations/ ./migrations/

EXPOSE 9000

CMD ["./server"]
