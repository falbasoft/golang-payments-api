package data

import (
  "testing"
  "reflect"

  "payments/models"
  "payments/fixtures"

  "github.com/go-openapi/strfmt"
)

// Create test payment object using fixture
func CreateTestPaymentFromFixture() (*models.Payment) {
  payment := models.Payment{}
  payment.UnmarshalBinary([]byte(fixtures.Payment))

  return &payment
}

// Compare payment objects
func PaymentObjectsEqual(src, dest *models.Payment) bool {
  // Ignore the ID which is auto generated
  src.ID = dest.ID

  in, _ :=  src.MarshalBinary()
  out, _ := dest.MarshalBinary()

  return reflect.DeepEqual(in, out)
}

// TestMain prepares database for testing
func TestMain(t *testing.T) {
  s, err := NewPostgresStore()

  if err != nil {
    t.Fatal("Could not connect to database", err)
  }

  store = *s
}

// TestInsertPayment: Test inserting a new payment into database
func TestInsertPayment(t *testing.T) {
  payment := CreateTestPaymentFromFixture()
  payment_id, err := store.InsertPayment(payment)

  if err != nil {
    t.Errorf("Error while inserting into database: '%v'", err)
  }

  inserted, err := store.GetPayment(payment_id)

  if err != nil {
    t.Errorf("Could not get inserted payment: '%v'", err)
  }

  if !PaymentObjectsEqual(inserted, payment) {
    t.Errorf("Got: '%v', expected: '%v'", inserted, payment)
  }

  store.DeletePayment(payment_id)
}

// TestDeletePayment: Test deleting payment from database
func TestDeletePayment(t *testing.T) {
  payment := CreateTestPaymentFromFixture()
  payment_id, err := store.InsertPayment(payment)

  if err != nil {
    t.Errorf("Error while creating test payment to be deleted: '%v'", err)
  }

  t.Run("Test deleting payment that doesn't exist in database", func(t *testing.T) {
    id := UUID()

    err = store.DeletePayment(&id)

    if err != ERR_PAYMENT_NOT_FOUND {
      t.Errorf("Didn't get expected error: '%v'", err)
    }

    _, err = store.GetPayment(&id)

    if err != ERR_PAYMENT_NOT_FOUND {
      t.Errorf("Payment has not been deleted")
    }
  })

  t.Run("Test deleting payment that exists in database", func(t *testing.T) {
    err = store.DeletePayment(payment_id)

    if err != nil {
      t.Errorf("Error while deleting payment: '%v'", err)
    }

    _, err = store.GetPayment(payment_id)

    if err != ERR_PAYMENT_NOT_FOUND {
      t.Errorf("Payment has not been deleted")
    }
  })
}

// TestGetPayment: Test getting a payment from database
func TestGetPayment(t *testing.T) {
  payment := CreateTestPaymentFromFixture()
  payment_id, err := store.InsertPayment(payment)

  if err != nil {
    t.Errorf("Error while inserting into database: '%v'", err)
  }

  t.Run("Test getting payment that doesn't exist in database", func(t *testing.T) {
    id := UUID()

    _, err := store.GetPayment(&id)

    if err != ERR_PAYMENT_NOT_FOUND {
      t.Errorf("Didn't get expected error: '%v'", err)
    }
  })

  t.Run("Test getting payment that exists in database", func(t *testing.T) {
    inserted, err := store.GetPayment(payment_id)

    if err != nil {
      t.Errorf("Could not get inserted payment: '%v'", err)
    }

    if !PaymentObjectsEqual(inserted, payment) {
      t.Errorf("Got: '%v', expected: '%v'", inserted, payment)
    }
  })

  store.DeletePayment(payment_id)
}

// TestGetPayments: Test getting a list of payments from database
func TestGetPayments(t *testing.T) {
  var payment_ids []*strfmt.UUID
  payment := CreateTestPaymentFromFixture()

  for i := 0; i < 5; i++ {
    payment_id, err := store.InsertPayment(payment)

    if err != nil {
      t.Errorf("Error while inserting into database: '%v'", err)
    }

    payment_ids = append(payment_ids, payment_id)
  }

  payments, err := store.GetPayments()

  if err != nil {
    t.Errorf("Error while getting payments: '%v'", err)
  }

  found := 0
  for _, op := range payments {
    for _, pid := range payment_ids {
      if op.ID == *pid {
        found++

        if !PaymentObjectsEqual(payment, op) {
          t.Errorf("Received payment is not the same as inserted one.")
        }
      }
    }
  }

  if found != 5 {
    t.Errorf("Inserted 5 payments, but found only %d.", found)
  }

  for _, pid := range payment_ids {
    store.DeletePayment(pid)
  }
}

// TestUpdatePayment: Test updating existing payment in database
func TestUpdatePayment(t *testing.T) {
  payment := CreateTestPaymentFromFixture()
  payment_id, err := store.InsertPayment(payment)

  payment.Attributes.Amount = "300"
  payment.Attributes.BeneficiaryParty.DebtorParty.SponsorParty.AccountNumber = "123443211234"

  t.Run("Test updating payment that doesn't exist in database", func(t *testing.T) {
    id := UUID()

    err = store.UpdatePayment(&id, payment)

    if err != ERR_PAYMENT_NOT_FOUND {
      t.Errorf("Didn't get expected error: '%v'", err)
    }
  })

  t.Run("Test updating payment that exists in database", func(t *testing.T) {
    err = store.UpdatePayment(payment_id, payment)

    if err != nil {
      t.Errorf("Error while updating payment: '%v'", err)
    }

    updated, err := store.GetPayment(payment_id)

    if err != nil {
      t.Errorf("Could not get updated payment: '%v'", err)
    }

    if !PaymentObjectsEqual(updated, payment) {
      t.Errorf("Got: '%v', expected: '%v'", updated, payment)
    }
  })

  store.DeletePayment(payment_id)
}

// TestPatchPayment: Test patching (partially updating) existing payment in database
func TestPatchPayment(t *testing.T) {
  payment := CreateTestPaymentFromFixture()
  payment_id, err := store.InsertPayment(payment)

  update := models.Payment{
    Attributes: &models.Attributes{
      Amount: "300",
      BeneficiaryParty: &models.BeneficiaryParty{
        DebtorParty: models.DebtorParty{
          SponsorParty: models.SponsorParty{
            AccountNumber: "123443211234",
          },
        },
      },
    },
  }

  payment.Attributes.Amount = "300"
  payment.Attributes.BeneficiaryParty.DebtorParty.SponsorParty.AccountNumber = "123443211234"

  t.Run("Test patching payment that doesn't exist in database", func(t *testing.T) {
    id := UUID()

    err = store.PatchPayment(&id, &update)

    if err != ERR_PAYMENT_NOT_FOUND {
      t.Errorf("Didn't get expected error: '%v'", err)
    }
  })

  t.Run("Test patching payment that exists in database", func(t *testing.T) {
    err = store.PatchPayment(payment_id, &update)

    if err != nil {
      t.Errorf("Error while updating payment: '%v'", err)
    }

    updated, err := store.GetPayment(payment_id)

    if err != nil {
      t.Errorf("Could not get updated payment: '%v'", err)
    }

    if !PaymentObjectsEqual(payment, updated) {
      t.Errorf("Got: '%v', expected: '%v'", updated, payment)
    }
  })

  store.DeletePayment(payment_id)
}
