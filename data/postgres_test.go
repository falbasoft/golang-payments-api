package data

import (
  "testing"

  _ "github.com/lib/pq"
)

var store PqStore

// Test connecting to postgres database
func TestConnectingToDatabase(t *testing.T) {
  pqStore, err := NewPostgresStore()

  if pqStore == nil && err != nil {
    t.Errorf("Encountered error: '%v' while connecting to database", err)
  }
}
