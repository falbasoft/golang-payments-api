package data

import (
  "errors"

  "payments/models"

  "github.com/twinj/uuid"
  "github.com/go-openapi/strfmt"
)

var ERR_PAYMENT_NOT_FOUND     = errors.New("Payment not found")
var ERR_BENEFICIARY_NOT_FOUND = errors.New("Beneficiary party not found")
var ERR_DEBTOR_NOT_FOUND      = errors.New("Debtor party not found")
var ERR_SPONSOR_NOT_FOUND     = errors.New("Sponsor party not found")

// DBPaymentData: A proxy type to store additional payment data
type DBPaymentData struct {
  BeneficiaryParty *models.DBBeneficiaryParty
  DebtorParty      *models.DBDebtorParty
  Sponsor          *models.DBSponsor
  SenderCharges    *[]models.DBSenderCharge
}

// UUID: Generate UUID
func UUID() strfmt.UUID {
  return strfmt.UUID(uuid.NewV4().String())
}

// ConvertAPIToDB: Converts API format into database tables format
func ConvertAPIToDB(payment *models.Payment) (*models.DBPayment, *DBPaymentData) {
  dbPayment := models.DBPayment{
    Organisation_id:           payment.OrganisationID,
    Type:                      payment.Type,
    Version:                   payment.Version,
    Amount:                    payment.Attributes.Amount,
    Bearer_code:               payment.Attributes.ChargesInformation.BearerCode,
    Receiver_charges_amount:   payment.Attributes.ChargesInformation.ReceiverChargesAmount,
    Receiver_charges_currency: payment.Attributes.ChargesInformation.ReceiverChargesCurrency,
    Currency:                  payment.Attributes.Currency,
    End_to_end_reference:      payment.Attributes.EndToEndReference,
    Contract_reference:        payment.Attributes.Fx.ContractReference,
    Exchange_rate:             payment.Attributes.Fx.ExchangeRate,
    Original_amount:           payment.Attributes.Fx.OriginalAmount,
    Original_currency:         payment.Attributes.Fx.OriginalCurrency,
    Numeric_reference:         payment.Attributes.NumericReference,
    Payment_id:                payment.Attributes.PaymentID,
    Payment_purpose:           payment.Attributes.PaymentPurpose,
    Payment_scheme:            payment.Attributes.PaymentScheme,
    Payment_type:              payment.Attributes.PaymentType,
    Processing_date:           payment.Attributes.ProcessingDate,
    Reference:                 payment.Attributes.Reference,
    Scheme_payment_sub_type:   payment.Attributes.SchemePaymentSubType,
    Scheme_payment_type:       payment.Attributes.SchemePaymentType,
  }

  var senderCharges []models.DBSenderCharge

  for _, senderCharge := range payment.Attributes.ChargesInformation.SenderCharges {
    senderCharges = append(senderCharges, models.DBSenderCharge{
      Amount:   senderCharge.Amount,
      Currency: senderCharge.Currency,
    })
  }

  dbPaymentData := DBPaymentData{
    BeneficiaryParty: &models.DBBeneficiaryParty{
      Account_type:  payment.Attributes.BeneficiaryParty.AccountType,
      DBDebtorParty: models.DBDebtorParty{
        Account_name:        payment.Attributes.BeneficiaryParty.AccountName,
        Account_number_code: payment.Attributes.BeneficiaryParty.AccountNumberCode,
        Address:             payment.Attributes.BeneficiaryParty.Address,
        Name:                payment.Attributes.BeneficiaryParty.Name,
        DBSponsor: models.DBSponsor{
          Account_number: payment.Attributes.BeneficiaryParty.AccountNumber,
          Bank_id:        payment.Attributes.BeneficiaryParty.BankID,
          Bank_id_code:   payment.Attributes.BeneficiaryParty.BankIDCode,
        },
      },
    },
    DebtorParty:      &models.DBDebtorParty{
      Account_name:        payment.Attributes.DebtorParty.AccountName,
      Account_number_code: payment.Attributes.DebtorParty.AccountNumberCode,
      Address:             payment.Attributes.DebtorParty.Address,
      Name:                payment.Attributes.DebtorParty.Name,
      DBSponsor: models.DBSponsor{
        Account_number: payment.Attributes.DebtorParty.SponsorParty.AccountNumber,
        Bank_id:        payment.Attributes.DebtorParty.SponsorParty.BankID,
        Bank_id_code:   payment.Attributes.DebtorParty.SponsorParty.BankIDCode,
      },
    },
    Sponsor:          &models.DBSponsor{
      Account_number:      payment.Attributes.SponsorParty.AccountNumber,
      Bank_id:             payment.Attributes.SponsorParty.BankID,
      Bank_id_code:        payment.Attributes.SponsorParty.BankIDCode,
    },
    SenderCharges:    &senderCharges,
  }

  return &dbPayment, &dbPaymentData
}

// InsertDBPayment: Inserts payment in a raw form into database
func (s *PqStore) InsertDBPayment(dbPayment *models.DBPayment, dbPaymentData *DBPaymentData) (*strfmt.UUID, error) {
  dbPaymentData.BeneficiaryParty.ID = UUID()

  query := `INSERT INTO payment_party (id,
                                       account_name,
                                       account_number,
                                       account_number_code,
                                       account_type,
                                       address,
                                       bank_id,
                                       bank_id_code,
                                       name)
                               VALUES (:id,
                                       :account_name,
                                       :account_number,
                                       :account_number_code,
                                       :account_type,
                                       :address,
                                       :bank_id,
                                       :bank_id_code,
                                       :name)`
  _, err := s.db.NamedExec(query, dbPaymentData.BeneficiaryParty)

  if err != nil {
    return nil, err
  }

  dbPaymentData.DebtorParty.ID = UUID()

  query = `INSERT INTO payment_party (id,
                                      account_name,
                                      account_number,
                                      account_number_code,
                                      address,
                                      bank_id,
                                      bank_id_code,
                                      name)
                              VALUES (:id,
                                      :account_name,
                                      :account_number,
                                      :account_number_code,
                                      :address,
                                      :bank_id,
                                      :bank_id_code,
                                      :name)`

  _, err = s.db.NamedExec(query, dbPaymentData.DebtorParty)

  if err != nil {
    return nil, err
  }

  dbPaymentData.Sponsor.ID = UUID()

  query = `INSERT INTO payment_party (id,
                                      account_number,
                                      bank_id,
                                      bank_id_code)
                              VALUES (:id,
                                      :account_number,
                                      :bank_id,
                                      :bank_id_code)`

  _, err = s.db.NamedExec(query, dbPaymentData.Sponsor)

  if err != nil {
    return nil, err
  }

  dbPayment.ID = UUID()
  dbPayment.Beneficiary_party_id = dbPaymentData.BeneficiaryParty.ID
  dbPayment.Debtor_party_id = dbPaymentData.DebtorParty.ID
  dbPayment.Sponsor_id = dbPaymentData.Sponsor.ID

  query = `INSERT INTO payment (id,
                                organisation_id,
                                type,
                                version,
                                amount,
                                beneficiary_party_id,
                                bearer_code,
                                receiver_charges_amount,
                                receiver_charges_currency,
                                currency,
                                debtor_party_id,
                                end_to_end_reference,
                                contract_reference,
                                exchange_rate,
                                original_amount,
                                original_currency,
                                numeric_reference,
                                payment_id,
                                payment_purpose,
                                payment_scheme,
                                payment_type,
                                processing_date,
                                reference,
                                scheme_payment_sub_type,
                                scheme_payment_type,
                                sponsor_id)
                         VALUES(:id,
                                :organisation_id,
                                :type,
                                :version,
                                :amount,
                                :beneficiary_party_id,
                                :bearer_code,
                                :receiver_charges_amount,
                                :receiver_charges_currency,
                                :currency,
                                :debtor_party_id,
                                :end_to_end_reference,
                                :contract_reference,
                                :exchange_rate,
                                :original_amount,
                                :original_currency,
                                :numeric_reference,
                                :payment_id,
                                :payment_purpose,
                                :payment_scheme,
                                :payment_type,
                                :processing_date,
                                :reference,
                                :scheme_payment_sub_type,
                                :scheme_payment_type,
                                :sponsor_id)`

  _, err = s.db.NamedExec(query, dbPayment)

  if err != nil {
    return nil, err
  }

  query = `INSERT INTO sender_charge (id,
                                      payment_id,
                                      amount,
                                      currency)
                              VALUES (:id,
                                      :payment_id,
                                      :amount,
                                      :currency)`

  for _, senderCharge := range *dbPaymentData.SenderCharges {
    senderCharge.ID         = UUID()
    senderCharge.Payment_id = dbPayment.ID

    _, err = s.db.NamedExec(query, senderCharge)

    if err != nil {
      return nil, err
    }
  }

  return &dbPayment.ID, nil
}

// DeleteDBPayment: Deletes payment and its data from the database
func (s *PqStore) DeleteDBPayment(id *strfmt.UUID) error {
  payment, err := s.GetDBPayment(id)

  if err != nil {
    return err
  }

  _, err = s.db.Exec("DELETE FROM sender_charge WHERE payment_id = $1", id)

  if err != nil {
    return err
  }

  _, err = s.db.Exec("DELETE FROM payment WHERE id = $1", id)

  if err != nil {
    return err
  }

  query := "DELETE FROM payment_party WHERE id = $1"

  _, err = s.db.Exec(query, payment.Beneficiary_party_id)

  if err != nil {
    return err
  }

  _, err = s.db.Exec(query, payment.Debtor_party_id)

  if err != nil {
    return err
  }

  _, err = s.db.Exec(query, payment.Sponsor_id)

  if err != nil {
    return err
  }

  return nil
}

// GetDBPayment: Get payment from database in a raw format
func (s *PqStore) GetDBPayment(id *strfmt.UUID) (*models.DBPayment, error) {
  var payment models.DBPayment

  err := s.db.Get(&payment, `SELECT * FROM payment WHERE id=$1`, id)

  if err != nil {
    return nil, ERR_PAYMENT_NOT_FOUND
  }

  return &payment, nil
}

// GetDBPaymentData: Get payment with data in raw format from database
func (s *PqStore) GetDBPaymentData(payment *models.DBPayment) (*DBPaymentData, error) {
  beneficiary_party, err := s.GetDBBeneficiaryParty(&payment.Beneficiary_party_id)

  if err != nil {
    return nil, err
  }

  debtor_party, err := s.GetDBDebtorParty(&payment.Debtor_party_id)

  if err != nil {
    return nil, err
  }

  sponsor, err := s.GetDBSponsor(&payment.Sponsor_id)

  if err != nil {
    return nil, err
  }

  sender_charges, err := s.GetDBSenderCharges(&payment.ID)

  if err != nil {
    return nil, err
  }

  return &DBPaymentData{beneficiary_party, debtor_party, sponsor, sender_charges}, nil
}

// GetDBBeneficiaryParty: Get beneficiary party from database in raw format
func (s *PqStore) GetDBBeneficiaryParty(id *strfmt.UUID) (*models.DBBeneficiaryParty, error) {
  var beneficiary_party models.DBBeneficiaryParty

  err := s.db.Get(&beneficiary_party, `SELECT * FROM payment_party WHERE id=$1`, id)

  if err != nil {
    return nil, ERR_BENEFICIARY_NOT_FOUND
  }

  return &beneficiary_party, nil
}

// GetDBDebtorParty: Get debtor party from database in raw format
func (s *PqStore) GetDBDebtorParty(id *strfmt.UUID) (*models.DBDebtorParty, error) {
  var debtor_party models.DBDebtorParty

  err := s.db.Get(&debtor_party, `SELECT ID, account_number, bank_id, bank_id_code, account_name, account_number_code, address, name FROM payment_party WHERE id=$1`, id)

  if err != nil {
    return nil, ERR_DEBTOR_NOT_FOUND
  }

  return &debtor_party, nil
}

// GetDBSponsor: Get sponsor party from database in raw format
func (s *PqStore) GetDBSponsor(id *strfmt.UUID) (*models.DBSponsor, error) {
  var sponsor models.DBSponsor

  err := s.db.Get(&sponsor, `SELECT ID, account_number, bank_id, bank_id_code FROM payment_party WHERE id=$1`, id)

  if err != nil {
    return nil, ERR_SPONSOR_NOT_FOUND
  }

  return &sponsor, nil
}

// GetDBSenderCharges: Get sender charges from database in raw format
func (s *PqStore) GetDBSenderCharges(id *strfmt.UUID) (*[]models.DBSenderCharge, error) {
  var sender_charges []models.DBSenderCharge

  err := s.db.Select(&sender_charges, `SELECT amount, currency FROM sender_charge WHERE payment_id=$1`, id)

  if err != nil {
    return nil, err
  }

  return &sender_charges, nil
}

// GetDBPayments: Gets a list of all payments registered in database
func (s *PqStore) GetDBPayments() (*[]models.DBPayment, error) {

  var dbPayments []models.DBPayment

  err := s.db.Select(&dbPayments, "SELECT * FROM payment")

  if err != nil {
    return nil, err
  }

  return &dbPayments, nil
}

// UpdateDBPayment: Updates payment and its data in a raw format in database
func (s *PqStore) UpdateDBPayment(id *strfmt.UUID, dbPayment *models.DBPayment, dbPaymentData *DBPaymentData) error {
  query := `UPDATE payment SET organisation_id           = :organisation_id,
                               type                      = :type,
                               version                   = :version,
                               amount                    = :amount,
                               bearer_code               = :bearer_code,
                               receiver_charges_amount   = :receiver_charges_amount,
                               receiver_charges_currency = :receiver_charges_currency,
                               currency                  = :currency,
                               end_to_end_reference      = :end_to_end_reference,
                               contract_reference        = :contract_reference,
                               exchange_rate             = :exchange_rate,
                               original_amount           = :original_amount,
                               original_currency         = :original_currency,
                               numeric_reference         = :numeric_reference,
                               payment_id                = :payment_id,
                               payment_purpose           = :payment_purpose,
                               payment_scheme            = :payment_scheme,
                               payment_type              = :payment_type,
                               processing_date           = :processing_date,
                               reference                 = :reference,
                               scheme_payment_sub_type   = :scheme_payment_sub_type,
                               scheme_payment_type       = :scheme_payment_type
            WHERE id = :id`

  dbPayment.ID = *id
  _, err := s.db.NamedExec(query, dbPayment)

  if err != nil {
    return err
  }

  query = `UPDATE payment_party SET account_name        = :account_name,
                                    account_number      = :account_number,
                                    account_number_code = :account_number_code,
                                    account_type        = :account_type,
                                    address             = :address,
                                    bank_id             = :bank_id,
                                    bank_id_code        = :bank_id_code,
                                    name                = :name
           WHERE id = (SELECT beneficiary_party_id FROM payment WHERE id = :id)`

  dbPaymentData.BeneficiaryParty.ID = *id
  _, err = s.db.NamedExec(query, dbPaymentData.BeneficiaryParty)
  dbPaymentData.BeneficiaryParty.ID = dbPayment.Beneficiary_party_id

  if err != nil {
    return err
  }

  query = `UPDATE payment_party SET account_name        = :account_name,
                                    account_number      = :account_number,
                                    account_number_code = :account_number_code,
                                    address             = :address,
                                    bank_id             = :bank_id,
                                    bank_id_code        = :bank_id_code,
                                    name                = :name
           WHERE id = (SELECT debtor_party_id FROM payment WHERE id = :id)`

  dbPaymentData.DebtorParty.ID = *id
  _, err = s.db.NamedExec(query, dbPaymentData.DebtorParty)
  dbPaymentData.DebtorParty.ID = dbPayment.Debtor_party_id

  if err != nil {
    return err
  }

  query = `UPDATE payment_party SET account_number      = :account_number,
                                    bank_id             = :bank_id,
                                    bank_id_code        = :bank_id_code
           WHERE id = (SELECT sponsor_id FROM payment WHERE id = :id)`

  dbPaymentData.Sponsor.ID = *id
  _, err = s.db.NamedExec(query, dbPaymentData.Sponsor)
  dbPaymentData.Sponsor.ID = dbPayment.Sponsor_id

  if err != nil {
    return err
  }

  _, err = s.db.Exec("DELETE FROM sender_charge WHERE payment_id = $1", id)

  if err != nil {
    return err
  }

  query = `INSERT INTO sender_charge (id,
                                      payment_id,
                                      amount,
                                      currency)
                              VALUES (:id,
                                      :payment_id,
                                      :amount,
                                      :currency)`

  for _, senderCharge := range *dbPaymentData.SenderCharges {
    senderCharge.ID         = UUID()
    senderCharge.Payment_id = dbPayment.ID

    _, err = s.db.NamedExec(query, senderCharge)

    if err != nil {
      return err
    }
  }

  return nil
}
