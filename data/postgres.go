package data

import (
  "os"
  "fmt"
  "strings"

  "github.com/jmoiron/sqlx"
  "github.com/jmoiron/sqlx/reflectx"

  _ "github.com/lib/pq"
)

// PqStore: Contains database connection and handling functions
type PqStore struct {
  db *sqlx.DB
}

// NewPostgresStore: Generate and return new PqStore
func NewPostgresStore() (*PqStore, error) {
  dbHost     := os.Getenv("POSTGRES_HOST")
  dbUser     := os.Getenv("POSTGRES_USER")
  dbPassword := os.Getenv("POSTGRES_PASSWORD")
  dbName     := os.Getenv("POSTGRES_DATABASE")

  connection := "postgres://%s:%s@%s/%s?sslmode=disable"

  db, err := sqlx.Connect("postgres", fmt.Sprintf(connection, dbUser, dbPassword, dbHost, dbName))
  if err != nil {
    return nil, err
  }

  // Use json tags instead of db tags
  db.Mapper = reflectx.NewMapperFunc("json", strings.ToLower)

  return &PqStore{
    db: db,
  }, nil
}
