package data

import (
  "testing"
  "reflect"

  "github.com/go-openapi/strfmt"

  "payments/models"
  "payments/fixtures"
)

// CreateTestDBPaymentFromFixture: Creates a raw test payment object using fixture
func CreateTestDBPaymentFromFixture() (*models.DBPayment, *DBPaymentData) {
  payment := models.Payment{}
  payment.UnmarshalBinary([]byte(fixtures.Payment))

  return ConvertAPIToDB(&payment)
}

// TestInsertDBPayment: Test inserting raw payment data into database
func TestInsertDBPayment(t *testing.T) {
  dbPayment, dbPaymentData := CreateTestDBPaymentFromFixture()
  payment_id, err := store.InsertDBPayment(dbPayment, dbPaymentData)

  if err != nil {
    t.Errorf("Error while inserting into database: '%v'", err)
  }

  inserted_dbPayment, err := store.GetDBPayment(payment_id)

  if err != nil {
    t.Errorf("Could not get inserted payment: '%v'", err)
  }

  if dbPayment.Processing_date.String() == inserted_dbPayment.Processing_date.String() {
    dbPayment.Processing_date = strfmt.Date{}
    inserted_dbPayment.Processing_date = strfmt.Date{}
  } else {
    t.Errorf("Processing dates don't match: original - '%v', inserted - '%v'", dbPayment.Processing_date, inserted_dbPayment.Processing_date)
  }

  if !reflect.DeepEqual(dbPayment, inserted_dbPayment) {
    t.Errorf("Got '%v' instead: '%v' while getting inserted payment", inserted_dbPayment, dbPayment)
  }

  inserted_dbPaymentData, err := store.GetDBPaymentData(inserted_dbPayment)

  if err != nil {
    t.Errorf("Could not get inserted payment data: '%v'", err)
  }

  if !reflect.DeepEqual(dbPaymentData, inserted_dbPaymentData) {
    t.Errorf("Got '%v' instead: '%v' while getting inserted payment data", inserted_dbPaymentData, dbPaymentData)
  }

  store.DeleteDBPayment(payment_id)
}

// TestDeleteDBPayment: Test deleting a payment from database
func TestDeleteDBPayment(t *testing.T) {
  dbPayment, dbPaymentData := CreateTestDBPaymentFromFixture()
  payment_id, err := store.InsertDBPayment(dbPayment, dbPaymentData)

  if err != nil {
    t.Errorf("Error while creating test payment to be deleted: '%v'", err)
  }

  t.Run("Test deleting payment that doesn't exist in database", func(t *testing.T) {
    id := UUID()

    err := store.DeleteDBPayment(&id)

    if err == nil {
      t.Errorf("Got '%v' instead of: '%v'", err, ERR_PAYMENT_NOT_FOUND)
    }
  })

  t.Run("Test deleting  payment that exists in database", func(t *testing.T) {
    err := store.DeleteDBPayment(payment_id)

    if err != nil {
      t.Errorf("Got error while deleting payment: '%v'", err)
    }

    _, err = store.GetDBPayment(payment_id)

    if err != ERR_PAYMENT_NOT_FOUND {
      t.Errorf("Payment has not been deleted")
    }
  })
}

// TestGetDBPayment: Test getting raw payment from database
func TestGetDBPayment(t *testing.T) {
  dbPayment, dbPaymentData := CreateTestDBPaymentFromFixture()
  payment_id, err := store.InsertDBPayment(dbPayment, dbPaymentData)

  if err != nil {
    t.Errorf("Error while creating test payment to be deleted: '%v'", err)
  }

  t.Run("Test getting a payment that exists in database", func(t *testing.T) {
    got, err := store.GetDBPayment(payment_id)

    if err != nil {
      t.Errorf("Error while getting a raw payment from database: '%v'", err)
    }

    if dbPayment.Processing_date.String() == got.Processing_date.String() {
      dbPayment.Processing_date = strfmt.Date{}
      got.Processing_date = strfmt.Date{}
    } else {
      t.Errorf("Processing dates don't match: original - '%v', updated - '%v'", dbPayment.Processing_date, got.Processing_date)
    }

    if !reflect.DeepEqual(dbPayment, got) {
      t.Errorf("Got '%v' instead: '%v' while getting payment", got, dbPayment)
    }
  })

  t.Run("Test getting a payment that doesn't exist in database", func(t *testing.T) {
    id := UUID()
    got, err := store.GetDBPayment(&id)

    if err != ERR_PAYMENT_NOT_FOUND {
      t.Errorf("Got '%v' instead of: '%v'", err, ERR_PAYMENT_NOT_FOUND)
    }

    if got != nil {
      _, err = store.GetDBPaymentData(got)

      if err == nil {
        t.Errorf("Got payment data instead of error")
      }
    }
  })

  store.DeleteDBPayment(payment_id)
}

// TestGetDBPayments: Test getting a list of raw payments from database
func TestGetDBPayments(t *testing.T) {
  var payment_ids []*strfmt.UUID
  dbPayment, dbPaymentData := CreateTestDBPaymentFromFixture()

  for i := 0; i < 5; i++ {
    payment_id, err := store.InsertDBPayment(dbPayment, dbPaymentData)

    if err != nil {
      t.Errorf("Error while inserting into database: '%v'", err)
    }

    payment_ids = append(payment_ids, payment_id)
  }

  dbPayments, err := store.GetDBPayments()

  if err != nil {
    t.Errorf("Error while getting payments: '%v'", err)
  }

  found := 0
  for _, op := range *dbPayments {
    for _, pid := range payment_ids {
      if op.ID == *pid {
        found++
      }
    }
  }

  if found != 5 {
    t.Errorf("Inserted 5 payments, but found only %d.", found)
  }

  for _, pid := range payment_ids {
    store.DeleteDBPayment(pid)
  }
}

// TestUpdateDBPayment: Test updating raw payment data in database 
func TestUpdateDBPayment(t *testing.T) {
  dbPayment, dbPaymentData := CreateTestDBPaymentFromFixture()
  payment_id, err := store.InsertDBPayment(dbPayment, dbPaymentData)

  dbPayment.Amount = "300"
  dbPaymentData.BeneficiaryParty.Account_number = "123443211234"

  err = store.UpdateDBPayment(payment_id, dbPayment, dbPaymentData)

  if err != nil {
    t.Errorf("Error while updating payment: '%v'", err)
  }

  updated_dbPayment, err := store.GetDBPayment(payment_id)

  if err != nil {
    t.Errorf("Could not get updated payment: '%v'", err)
  }

  if dbPayment.Processing_date.String() == updated_dbPayment.Processing_date.String() {
    dbPayment.Processing_date = strfmt.Date{}
    updated_dbPayment.Processing_date = strfmt.Date{}
  } else {
    t.Errorf("Processing dates don't match: original - '%v', updated - '%v'", dbPayment.Processing_date, updated_dbPayment.Processing_date)
  }

  if !reflect.DeepEqual(dbPayment, updated_dbPayment) {
    t.Errorf("Got '%v' instead: '%v' while getting updated payment", updated_dbPayment, dbPayment)
  }

  updated_dbPaymentData, err := store.GetDBPaymentData(updated_dbPayment)

  if err != nil {
    t.Errorf("Could not get updated payment data: '%v'", err)
  }

  if !reflect.DeepEqual(dbPaymentData, updated_dbPaymentData) {
    t.Errorf("Got '%v' instead: '%v' while getting updated payment", updated_dbPaymentData, dbPaymentData)
  }

  store.DeleteDBPayment(payment_id)
}
