package data

import (
  "github.com/go-openapi/strfmt"

  "payments/models"
)

// ConverTDBToAPI: Convert database model into API format
func ConvertDBToAPI(
  dbPayment *models.DBPayment,
  dbPaymentData *DBPaymentData,
) *models.Payment {
  beneficiary_party := dbPaymentData.BeneficiaryParty
  debtor_party      := dbPaymentData.DebtorParty
  sponsor           := dbPaymentData.Sponsor
  sender_charges    := dbPaymentData.SenderCharges

  var SenderCharges []*models.SenderCharge

  for _, sender_charge := range *sender_charges {
    SenderCharges = append(SenderCharges, &models.SenderCharge{
      Amount:   sender_charge.Amount,
      Currency: sender_charge.Currency,
    })
  }

  return &models.Payment{
    ID:             dbPayment.ID,
    OrganisationID: dbPayment.Organisation_id,
    Type:           dbPayment.Type,
    Version:        dbPayment.Version,
    Attributes:     &models.Attributes{
      Amount:               dbPayment.Amount,
      BeneficiaryParty:     &models.BeneficiaryParty{
        AccountType: beneficiary_party.Account_type,
        DebtorParty: models.DebtorParty{
          AccountName:       beneficiary_party.Account_name,
          AccountNumberCode: beneficiary_party.Account_number_code,
          Address:           beneficiary_party.Address,
          Name:              beneficiary_party.Name,
          SponsorParty:      models.SponsorParty{
            AccountNumber: beneficiary_party.Account_number,
            BankID:        beneficiary_party.Bank_id,
            BankIDCode:    beneficiary_party.Bank_id_code,
          },
        },
      },
      ChargesInformation: &models.ChargesInformation{
        BearerCode:              dbPayment.Bearer_code,
        ReceiverChargesAmount:   dbPayment.Receiver_charges_amount,
        ReceiverChargesCurrency: dbPayment.Receiver_charges_currency,
        SenderCharges: SenderCharges,
      },
      Currency:             dbPayment.Currency,
      DebtorParty:          &models.DebtorParty{
        AccountName:       debtor_party.Account_name,
        AccountNumberCode: debtor_party.Account_number_code,
        Address:           debtor_party.Address,
        Name:              debtor_party.Name,
        SponsorParty:      models.SponsorParty{
          AccountNumber: debtor_party.Account_number,
          BankID:        debtor_party.Bank_id,
          BankIDCode:    debtor_party.Bank_id_code,
        },
      },
      EndToEndReference:    dbPayment.End_to_end_reference,
      Fx:                   &models.Fx{
        ContractReference: dbPayment.Contract_reference,
        ExchangeRate:      dbPayment.Exchange_rate,
        OriginalAmount:    dbPayment.Original_amount,
        OriginalCurrency:  dbPayment.Original_currency,
      },
      NumericReference:     dbPayment.Numeric_reference,
      PaymentID:            dbPayment.Payment_id,
      PaymentPurpose:       dbPayment.Payment_purpose,
      PaymentScheme:        dbPayment.Payment_scheme,
      PaymentType:          dbPayment.Payment_type,
      ProcessingDate:       dbPayment.Processing_date,
      Reference:            dbPayment.Reference,
      SchemePaymentSubType: dbPayment.Scheme_payment_sub_type,
      SchemePaymentType:    dbPayment.Scheme_payment_type,
      SponsorParty:         &models.SponsorParty{
        AccountNumber:     sponsor.Account_number,
        BankID:            sponsor.Bank_id,
        BankIDCode:        sponsor.Bank_id_code,
      },
    },
  }
}

// InsertPayment: Inserts payment in API format into database
func (s *PqStore) InsertPayment(payment *models.Payment) (*strfmt.UUID, error) {
  dbPayment, dbPaymentData := ConvertAPIToDB(payment)

  return s.InsertDBPayment(dbPayment, dbPaymentData)
}

// DeletePayment: Deletes payment from database
func (s *PqStore) DeletePayment(id *strfmt.UUID) error {
  return s.DeleteDBPayment(id)
}

// GetPayment: Gets a specified payment in API format from database
func (s *PqStore) GetPayment(id *strfmt.UUID) (*models.Payment, error) {
  dbPayment, err := s.GetDBPayment(id)

  if err != nil {
    return nil, err
  }

  dbPaymentData, err := s.GetDBPaymentData(dbPayment)

  if err != nil {
    return nil, err
  }

  return ConvertDBToAPI(dbPayment, dbPaymentData), nil
}

// GetPayments: Gets a list of all payments in API format from the database
func (s *PqStore) GetPayments() ([]*models.Payment, error) {
  var payments []*models.Payment

  dbPayments, err := s.GetDBPayments()

  if err != nil {
    return nil, err
  }

  for _, dbPayment := range *dbPayments {
    dbPaymentData, err := s.GetDBPaymentData(&dbPayment)

    if err != nil {
      return nil, err
    }

    payments = append(payments, ConvertDBToAPI(&dbPayment, dbPaymentData))
  }

  return payments, nil
}

// UpdatePayment: Updates payment in API format in database
func (s *PqStore) UpdatePayment(id *strfmt.UUID, payment *models.Payment) error {
  dbPayment, err:= s.GetDBPayment(id)

  if err != nil {
    return err
  }

  dbPayment, dbPaymentData := ConvertAPIToDB(payment)

  return s.UpdateDBPayment(id, dbPayment, dbPaymentData)
}

// PatchPayment: Patches (Partially Updates) payment in API format in database
func (s *PqStore) PatchPayment(id *strfmt.UUID, payment *models.Payment) error {
  dbPayment, err:= s.GetDBPayment(id)

  if err != nil {
    return err
  }

  if payment.OrganisationID != "" {
    dbPayment.Organisation_id = payment.OrganisationID
  }

  if payment.Type != "" {
    dbPayment.Type = payment.Type
  }

  if payment.Version != 0 {
    dbPayment.Version = payment.Version
  }

  if payment.Attributes != nil {
    if payment.Attributes.Amount != "" {
      dbPayment.Amount = payment.Attributes.Amount
    }

    if payment.Attributes.Currency != "" {
      dbPayment.Currency = payment.Attributes.Currency
    }

    if payment.Attributes.EndToEndReference != "" {
      dbPayment.End_to_end_reference = payment.Attributes.EndToEndReference
    }

    if payment.Attributes.NumericReference != "" {
      dbPayment.Numeric_reference = payment.Attributes.NumericReference
    }

    if payment.Attributes.PaymentID != "" {
      dbPayment.Payment_id = payment.Attributes.PaymentID
    }

    if payment.Attributes.PaymentPurpose != "" {
      dbPayment.Payment_purpose = payment.Attributes.PaymentPurpose
    }

    if payment.Attributes.PaymentScheme != "" {
      dbPayment.Payment_scheme = payment.Attributes.PaymentScheme
    }

    if payment.Attributes.PaymentType != "" {
      dbPayment.Payment_type = payment.Attributes.PaymentType
    }

    if payment.Attributes.ProcessingDate.String() != "0001-01-01" {
      dbPayment.Processing_date = payment.Attributes.ProcessingDate
    }

    if payment.Attributes.Reference != "" {
      dbPayment.Reference = payment.Attributes.Reference
    }

    if payment.Attributes.Reference != "" {
      dbPayment.Reference = payment.Attributes.Reference
    }

    if payment.Attributes.SchemePaymentSubType != "" {
      dbPayment.Scheme_payment_sub_type = payment.Attributes.SchemePaymentSubType
    }

    if payment.Attributes.SchemePaymentType != "" {
      dbPayment.Scheme_payment_type = payment.Attributes.SchemePaymentType
    }

    if payment.Attributes.ChargesInformation != nil {
      if payment.Attributes.ChargesInformation.BearerCode != "" {
        dbPayment.Bearer_code = payment.Attributes.ChargesInformation.BearerCode
      }

      if payment.Attributes.ChargesInformation.ReceiverChargesAmount != "" {
        dbPayment.Receiver_charges_amount = payment.Attributes.ChargesInformation.ReceiverChargesAmount
      }

      if payment.Attributes.ChargesInformation.ReceiverChargesCurrency != "" {
        dbPayment.Receiver_charges_currency = payment.Attributes.ChargesInformation.ReceiverChargesCurrency
      }
    }

    if payment.Attributes.Fx != nil {
      if payment.Attributes.Fx.ContractReference != "" {
        dbPayment.Contract_reference = payment.Attributes.Fx.ContractReference
      }

      if payment.Attributes.Fx.ExchangeRate != "" {
        dbPayment.Exchange_rate = payment.Attributes.Fx.ExchangeRate
      }

      if payment.Attributes.Fx.OriginalAmount != "" {
        dbPayment.Original_amount = payment.Attributes.Fx.OriginalAmount
      }

      if payment.Attributes.Fx.OriginalCurrency != "" {
        dbPayment.Original_currency = payment.Attributes.Fx.OriginalCurrency
      }
    }
  }

  dbPaymentData, err := s.GetDBPaymentData(dbPayment)

  if err != nil {
    return nil
  }

  if payment.Attributes.BeneficiaryParty != nil {
    if payment.Attributes.BeneficiaryParty.AccountNumber != "" {
      dbPaymentData.BeneficiaryParty.Account_number = payment.Attributes.BeneficiaryParty.AccountNumber
    }

    if payment.Attributes.BeneficiaryParty.BankID != "" {
      dbPaymentData.BeneficiaryParty.Bank_id = payment.Attributes.BeneficiaryParty.BankID
    }

    if payment.Attributes.BeneficiaryParty.BankIDCode != "" {
      dbPaymentData.BeneficiaryParty.Bank_id_code = payment.Attributes.BeneficiaryParty.BankIDCode
    }

    if payment.Attributes.BeneficiaryParty.AccountName != "" {
      dbPaymentData.BeneficiaryParty.Account_name = payment.Attributes.BeneficiaryParty.AccountName
    }

    if payment.Attributes.BeneficiaryParty.AccountNumberCode != "" {
      dbPaymentData.BeneficiaryParty.Account_number_code = payment.Attributes.BeneficiaryParty.AccountNumberCode
    }

    if payment.Attributes.BeneficiaryParty.Address != "" {
      dbPaymentData.BeneficiaryParty.Address = payment.Attributes.BeneficiaryParty.Address
    }

    if payment.Attributes.BeneficiaryParty.Name != "" {
      dbPaymentData.BeneficiaryParty.Name = payment.Attributes.BeneficiaryParty.Name
    }

    if payment.Attributes.BeneficiaryParty.Name != "" {
      dbPaymentData.BeneficiaryParty.Name = payment.Attributes.BeneficiaryParty.Name
    }

    if payment.Attributes.BeneficiaryParty.AccountType != 0 {
      dbPaymentData.BeneficiaryParty.Account_type = payment.Attributes.BeneficiaryParty.AccountType
    }
  }

  if payment.Attributes.DebtorParty != nil {
    if payment.Attributes.DebtorParty.AccountNumber != "" {
      dbPaymentData.DebtorParty.Account_number = payment.Attributes.DebtorParty.AccountNumber
    }

    if payment.Attributes.DebtorParty.BankID != "" {
      dbPaymentData.DebtorParty.Bank_id = payment.Attributes.DebtorParty.BankID
    }

    if payment.Attributes.DebtorParty.BankIDCode != "" {
      dbPaymentData.DebtorParty.Bank_id_code = payment.Attributes.DebtorParty.BankIDCode
    }

    if payment.Attributes.DebtorParty.AccountName != "" {
      dbPaymentData.DebtorParty.Account_name = payment.Attributes.DebtorParty.AccountName
    }

    if payment.Attributes.DebtorParty.AccountNumberCode != "" {
      dbPaymentData.DebtorParty.Account_number_code = payment.Attributes.DebtorParty.AccountNumberCode
    }

    if payment.Attributes.DebtorParty.Address != "" {
      dbPaymentData.DebtorParty.Address = payment.Attributes.DebtorParty.Address
    }

    if payment.Attributes.DebtorParty.Name != "" {
      dbPaymentData.DebtorParty.Name = payment.Attributes.DebtorParty.Name
    }

    if payment.Attributes.DebtorParty.Name != "" {
      dbPaymentData.DebtorParty.Name = payment.Attributes.DebtorParty.Name
    }
  }

  if payment.Attributes.SponsorParty != nil {
    if payment.Attributes.SponsorParty.AccountNumber != "" {
      dbPaymentData.Sponsor.Account_number = payment.Attributes.SponsorParty.AccountNumber
    }

    if payment.Attributes.SponsorParty.BankID != "" {
      dbPaymentData.Sponsor.Bank_id = payment.Attributes.SponsorParty.BankID
    }

    if payment.Attributes.DebtorParty.BankIDCode != "" {
      dbPaymentData.Sponsor.Bank_id_code = payment.Attributes.SponsorParty.BankIDCode
    }
  }

  return s.UpdateDBPayment(id, dbPayment, dbPaymentData)
}
