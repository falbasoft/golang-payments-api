// Code generated by go-swagger; DO NOT EDIT.

/*
Package restapi Form3 Payments API
Form3 Payments API


    Schemes:
      http
    Host: virtserver.swaggerhub.com
    BasePath: /v1
    Version: v1

    Consumes:
    - application/json

    Produces:
    - application/json

swagger:meta
*/
package restapi
