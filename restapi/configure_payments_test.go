package restapi

import (
  "testing"
  "reflect"
  "bytes"

  "io/ioutil"
  "net/http"
  "net/http/httptest"

  "payments/restapi/operations"
  "payments/fixtures"
  "payments/models"
  "payments/data"

  "github.com/go-openapi/strfmt"
)

var testServer *httptest.Server
var client http.Client

// GetErrorFromJSON: Converts JSON to error message
func GetErrorFromJSON(json []byte) string {
  err := models.Error{}
  err.UnmarshalBinary(json)

  return string(err.Error)
}

// Create test payment object using fixture
func CreateTestPaymentFromFixture() (*models.Payment) {
  payment := models.Payment{}
  payment.UnmarshalBinary([]byte(fixtures.Payment))

  return &payment
}

// AssertObjectsEqual: Assert two payment objects and check if they're equal
func AssertObjectsEqual(t *testing.T, msg string, i, o *models.Payment) {
  // ignore the ID which is auto generated
  i.ID = o.ID

  in, _  :=  i.MarshalBinary()
  out, _ := o.MarshalBinary()

  if !reflect.DeepEqual(in, out) {
    t.Errorf(msg)
  }
}

// MakeRequest: Make a request using HTTP client
func MakeRequest(method, url string, data []byte) ([]byte, error) {
  req, err := http.NewRequest(method, url, bytes.NewBuffer(data))

  if err != nil {
    return nil, err
  }

  req.Header.Set("Content-Type", "application/json")

  res, err := client.Do(req)

  if err != nil {
    return nil, err
  }

  body, err := ioutil.ReadAll(res.Body)

  if err != nil {
    return nil, err
  }

  return body, nil
}

// Test Initialisation
func TestMain(t *testing.T) {
  handler, err := GetAPIHandler()

  if err != nil {
    t.Fatal("Could not get API Handler", err)
  }

  testServer = httptest.NewServer(handler)
}

// TestAddingPayment: Test Adding a new payment in system
func TestAddingPayment(t *testing.T) {
  // Post fixture payment to /payments
  res, err := MakeRequest(http.MethodPost, testServer.URL + "/v1/payments", []byte(fixtures.Payment))

  if err != nil {
    t.Errorf("HTTP Request Error: %v", err)
  }

  // Interpret API output as Payment structure
  api_output := operations.GetPaymentOKBody{}
  api_output.UnmarshalBinary(res)

  // Get added payment from the database
  db_output, err := store.GetPayment(&api_output.Payment.ID)

  if err != nil {
    t.Errorf("Database Error: %v", err)
  }

  // Compare API output with the contents of database
  AssertObjectsEqual(t, "Returned payment is not the same as the one in the database", api_output.Payment, db_output)

  // Compare API input (fixture) with the contents of database
  AssertObjectsEqual(t, "Returned payment is not the same as the one in the database", CreateTestPaymentFromFixture(), db_output)

  // Clean up
  store.DeletePayment(&api_output.Payment.ID)
}

// TestDeletingPayment: Test updating specified payment in the system
func TestDeletingPayment(t *testing.T) {
  payment := CreateTestPaymentFromFixture()
  payment_id, _ := store.InsertPayment(payment)

  t.Run("Deleting a payment that exists in database", func(t *testing.T) {
    // Call the API
    _, err := MakeRequest(http.MethodDelete, testServer.URL + "/v1/payments/" + string(*payment_id), []byte{})

    if err != nil {
      t.Errorf("HTTP Request Error: %v", err)
    }

    _, err = store.GetPayment(payment_id)

    if err != data.ERR_PAYMENT_NOT_FOUND {
      t.Errorf("Payment has not been properly deleted.")
    }
  })

  t.Run("Deleting payment that doesn't exist in database", func(t *testing.T) {
    // Call the API
    res, err := MakeRequest(http.MethodDelete, testServer.URL + "/v1/payments/" + string(data.UUID()), []byte{})

    if err != nil {
      t.Errorf("HTTP Request Error: %v", err)
    }

    // Interpret API output as error message
    msg := GetErrorFromJSON(res)

    if msg != string(data.ERR_PAYMENT_NOT_FOUND.Error()) {
      t.Errorf("Wrong error response: %s", msg)
    }
  })

  store.DeletePayment(payment_id)
}

// TestGettingPayment: Test getting specified payment from the system
func TestGettingPayment(t *testing.T) {
  // Prepare test data
  payment := CreateTestPaymentFromFixture()
  payment_id, _ := store.InsertPayment(payment)
  payment.ID = *payment_id

  t.Run("Getting a payment that exists in database", func(t *testing.T) {
    // Call the API
    res, err := MakeRequest(http.MethodGet, testServer.URL + "/v1/payments/" + string(*payment_id), []byte{})

    if err != nil {
      t.Errorf("HTTP Request Error: %v", err)
    }

    // Interpret API output as Payment structure
    api_output := operations.GetPaymentOKBody{}
    api_output.UnmarshalBinary(res)

    AssertObjectsEqual(t, "Returned payment is not the same as the one in the database", payment, api_output.Payment)
  })

  t.Run("Getting a payment that doesn't exist in database", func(t *testing.T) {
    // Call the API
    res, err := MakeRequest(http.MethodGet, testServer.URL + "/v1/payments/" + string(data.UUID()), []byte{})

    if err != nil {
      t.Errorf("HTTP Request Error: %v", err)
    }

    // Interpret API output as error message
    msg := GetErrorFromJSON(res)

    if msg != string(data.ERR_PAYMENT_NOT_FOUND.Error()) {
      t.Errorf("Wrong error response: %s", msg)
    }
  })

  store.DeletePayment(payment_id)
}

// TestGetPayments: Test getting a list of all payments
func TestGetPayments(t *testing.T) {
  // Prepare test data
  payment := CreateTestPaymentFromFixture()
  payment_ids := []*strfmt.UUID{}

  for i := 0; i < 5; i++ {
    payment_id, _ := store.InsertPayment(payment)
    payment_ids = append(payment_ids, payment_id)
  }

  // Call the API
  res, err := MakeRequest(http.MethodGet, testServer.URL + "/v1/payments", []byte{})

  if err != nil {
    t.Errorf("HTTP Request Error: %v", err)
  }

  // Interpret API output as Payment structure

  var output operations.GetPaymentsOKBody

  output.UnmarshalBinary(res)

  found := 0
  for _, op := range output.Data {
    for _, pid := range payment_ids {
      if op.ID == *pid {
        found++

        AssertObjectsEqual(t, "Returned payment is not the same as the one in the database", payment, op)
      }
    }
  }

  if found != 5 {
    t.Errorf("Inserted 5 payments, but got only %d out of them.", found)
  }

  for _, pid := range payment_ids {
    store.DeletePayment(pid)
  }
}

// TestPatchingPayment: Test patching (partially updating) specified payment in the system
func TestPatchingPayment(t *testing.T) {
  payment := CreateTestPaymentFromFixture()
  payment_id, _ := store.InsertPayment(payment)

  update := models.Payment{
    Attributes: &models.Attributes{
      Amount: "300",
      BeneficiaryParty: &models.BeneficiaryParty{
        DebtorParty: models.DebtorParty{
          SponsorParty: models.SponsorParty{
            AccountNumber: "123443211234",
          },
        },
      },
    },
  }

  payment.Attributes.Amount = "300"
  payment.Attributes.BeneficiaryParty.DebtorParty.SponsorParty.AccountNumber = "123443211234"

  json, err := update.MarshalBinary()

  if err != nil {
    t.Errorf("JSON Error: %v", err)
  }

  t.Run("Patching a payment that exists in database", func(t *testing.T) {
    // Call the API
    res, err := MakeRequest(http.MethodPatch, testServer.URL + "/v1/payments/" + string(*payment_id), json)

    if err != nil {
      t.Errorf("HTTP Request Error: %v", err)
    }

    api_output := operations.PatchPaymentOKBody{}
    api_output.UnmarshalBinary(res)

    AssertObjectsEqual(t, "Returned payment has not been patched properly", payment, api_output.Payment)
  })

  t.Run("Patching a payment that doesn't exists in database", func(t *testing.T) {
    // Call the API
    res, err := MakeRequest(http.MethodPatch, testServer.URL + "/v1/payments/" + string(data.UUID()), json)

    if err != nil {
      t.Errorf("HTTP Request Error: %v", err)
    }

    // Interpret API output as error message
    msg := GetErrorFromJSON(res)

    if msg != string(data.ERR_PAYMENT_NOT_FOUND.Error()) {
      t.Errorf("Wrong error response: %s", msg)
    }
  })
}

// TestUpdatingPayment: Test updating specified payment in the system
func TestUpdatingPayment(t *testing.T) {
  payment := CreateTestPaymentFromFixture()
  payment_id, _ := store.InsertPayment(payment)

  payment.Attributes.Amount = "300"
  payment.Attributes.BeneficiaryParty.DebtorParty.SponsorParty.AccountNumber = "123443211234"

  json, err := payment.MarshalBinary()

  if err != nil {
    t.Errorf("JSON Error: %v", err)
  }

  t.Run("Updating a payment that exists in database", func(t *testing.T) {
    // Call the API
    res, err := MakeRequest(http.MethodPut, testServer.URL + "/v1/payments/" + string(*payment_id), json)

    if err != nil {
      t.Errorf("HTTP Request Error: %v", err)
    }

    // Interpret API output as Payment structure
    api_output := operations.UpdatePaymentOKBody{}
    api_output.UnmarshalBinary(res)

    AssertObjectsEqual(t, "Returned payment has not been updated properly", payment, api_output.Payment)
  })

  t.Run("Updating payment that doesn't exist in database", func(t *testing.T) {
    // Call the API
    res, err := MakeRequest(http.MethodPut, testServer.URL + "/v1/payments/" + string(data.UUID()), json)

    if err != nil {
      t.Errorf("HTTP Request Error: %v", err)
    }

    // Interpret API output as error message
    msg := GetErrorFromJSON(res)

    if msg != string(data.ERR_PAYMENT_NOT_FOUND.Error()) {
      t.Errorf("Wrong error response: %s", msg)
    }
  })

  store.DeletePayment(payment_id)
}
