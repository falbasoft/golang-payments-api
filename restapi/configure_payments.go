// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
  "crypto/tls"
  "net/http"

  "log"

  errors "github.com/go-openapi/errors"
  runtime "github.com/go-openapi/runtime"
  middleware "github.com/go-openapi/runtime/middleware"

  "payments/restapi/operations"
  "payments/models"
  "payments/data"
)

var store *data.PqStore

// create database store (connection) on init
func init() {
  s, err := data.NewPostgresStore()

  if err != nil {
    log.Fatalf("Could not connect to the database: %v", err)
  }

  store = s
}

//go:generate swagger generate server --target ../../payments-api --name Payments --spec ../swagger.yml

func configureFlags(api *operations.PaymentsAPI) {
  // api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

func getLinks(req *http.Request) *models.Links {
  return &models.Links{"http://" + req.Host + req.URL.String()}
}

func configureAPI(api *operations.PaymentsAPI) http.Handler {
  // configure the api here
  api.ServeError = errors.ServeError

  // Set your custom logger if needed. Default one is log.Printf
  // Expected interface func(string, ...interface{})
  //
  // Example:
  // api.Logger = log.Printf

  api.JSONConsumer = runtime.JSONConsumer()

  api.JSONProducer = runtime.JSONProducer()

  api.AddPaymentHandler = operations.AddPaymentHandlerFunc(func(params operations.AddPaymentParams) middleware.Responder {
    var links = getLinks(params.HTTPRequest)

    id, err := store.InsertPayment(params.Payment)

    if err != nil {
      log.Printf("AddPayment error: %v", err)

      return operations.NewAddPaymentBadRequest()
    }

    payment, err := store.GetPayment(id)

    if err != nil {
      log.Printf("AddPayment error: %v", err)

      return operations.NewAddPaymentBadRequest()
    }

    return operations.NewAddPaymentOK().WithPayload(&operations.AddPaymentOKBody{
      links,
      payment,
    })
  })

  api.DeletePaymentHandler = operations.DeletePaymentHandlerFunc(func(params operations.DeletePaymentParams) middleware.Responder {
    err := store.DeletePayment(&params.ID)

    if err != nil {
      log.Printf("DeletePayment error: %v", err)

      return operations.NewDeletePaymentNotFound().WithPayload(&models.Error{err.Error()})
    }

    return operations.NewDeletePaymentCreated()
  })

  api.GetPaymentHandler = operations.GetPaymentHandlerFunc(func(params operations.GetPaymentParams) middleware.Responder {
    var links = getLinks(params.HTTPRequest)

    payment, err := store.GetPayment(&params.ID)

    if err != nil {
      log.Printf("GetPayment error: %v", err)

      return operations.NewGetPaymentNotFound().WithPayload(&models.Error{err.Error()})
    }

    return operations.NewGetPaymentOK().WithPayload(&operations.GetPaymentOKBody{
      links,
      payment,
    })
  })

  api.GetPaymentsHandler = operations.GetPaymentsHandlerFunc(func(params operations.GetPaymentsParams) middleware.Responder {
    var links = getLinks(params.HTTPRequest)

    payments, err := store.GetPayments()

    if err != nil {
      log.Printf("GetPayments error: %v", err)

      return operations.NewGetPaymentsOK().WithPayload(&operations.GetPaymentsOKBody{
        []*models.Payment{},
        links,
      })
    }

    return operations.NewGetPaymentsOK().WithPayload(&operations.GetPaymentsOKBody{
      payments,
      links,
    })
  })

  api.PatchPaymentHandler = operations.PatchPaymentHandlerFunc(func(params operations.PatchPaymentParams) middleware.Responder {
    var links = getLinks(params.HTTPRequest)

    id := &params.ID

    _, err := store.GetPayment(id)

    if err != nil {
      log.Printf("UpdatePayment error: %v", err);

      return operations.NewPatchPaymentNotFound().WithPayload(&models.Error{err.Error()})
    }

    err = store.PatchPayment(id, params.Payment)

    if err != nil {
      log.Printf("UpdatePayment error: %v", err);

      return operations.NewPatchPaymentNotFound().WithPayload(&models.Error{err.Error()})
    }

    payment, _ := store.GetPayment(id)

    return operations.NewPatchPaymentOK().WithPayload(&operations.PatchPaymentOKBody{
      links,
      payment,
    })
  })

  api.UpdatePaymentHandler = operations.UpdatePaymentHandlerFunc(func(params operations.UpdatePaymentParams) middleware.Responder {
    var links = getLinks(params.HTTPRequest)

    id := &params.ID

    _, err := store.GetPayment(id)

    if err != nil {
      log.Printf("UpdatePayment error: %v", err);

      return operations.NewUpdatePaymentNotFound().WithPayload(&models.Error{err.Error()})
    }

    err = store.UpdatePayment(id, params.Payment)

    if err != nil {
      log.Printf("UpdatePayment error: %v", err);

      return operations.NewUpdatePaymentNotFound().WithPayload(&models.Error{err.Error()})
    }

    payment, _ := store.GetPayment(id)

    return operations.NewUpdatePaymentOK().WithPayload(&operations.UpdatePaymentOKBody{
      links,
      payment,
    })
  })

  api.ServerShutdown = func() {}

  return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
  // Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix"
func configureServer(s *http.Server, scheme, addr string) {
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation
func setupMiddlewares(handler http.Handler) http.Handler {
  return handler
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics
func setupGlobalMiddleware(handler http.Handler) http.Handler {
  return handler
}
