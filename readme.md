### Running Service

To run the service you only need to clone the repository and do `docker-compose up`.
This will:

1. Build the binaries for database migrations and API web server
2. Set up containers
3. Migrate database
4. Run the service

Service will then be accessible via http://localhost:9000 url.

### Documentation

Additional documentation is available here:
- [Database Schema](docs/db.pdf)
- [API Specification](docs/api.pdf)

### Solution Architecture
```
             API user
              |    ^
              v    |
(View)       HTTP API Front end (Swagger-Go)
              |    ^
              v    |
(Controller) Database Store API
              |    ^
              v    |
(Model)      Database Store
              |    ^
              v    |
             Database (PostgreSQL)
```

### Solution Steps Taken:
- Define API in Swagger OpenAPI
- Generate server skeleton using Swagger-Go
- Write behaviour tests for HTTP API
- Define database schema
- Write unit tests for database store handling routines
- Write database store handling routines
- Write unit tests for database store API
- Write database store API
- Connect database store API to HTTP front generated by Swagger-Go
- Make sure tests pass
- Containerize (docker, docker-compose)
