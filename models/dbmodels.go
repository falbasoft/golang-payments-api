package models

import "github.com/go-openapi/strfmt"

type DBPayment struct {
  ID                        strfmt.UUID `db: "id"`
  Organisation_id           strfmt.UUID `db: "organisation_id"`
  Type                      string      `db: "type"`
  Version                   int64       `db: "version"`
  Amount                    string      `db: "amount"`
  Beneficiary_party_id      strfmt.UUID `db: "beneficiary_party_id"`
  Bearer_code               string      `db: "bearer_code"`
  Receiver_charges_amount   string      `db: "receiver_charges_amount"`
  Receiver_charges_currency string      `db: "receiver_charges_currency"`
  Currency                  string      `db: "currency"`
  Debtor_party_id           strfmt.UUID `db: "debtor_party_id"`
  End_to_end_reference      string      `db: "end_to_end_reference"`
  Contract_reference        string      `db: "contract_reference"`
  Exchange_rate             string      `db: "exchange_rate"`
  Original_amount           string      `db: "original_amount"`
  Original_currency         string      `db: "original_currency"`
  Numeric_reference         string      `db: "numeric_reference"`
  Payment_id                string      `db: "payment_id"`
  Payment_purpose           string      `db: "payment_purpose"`
  Payment_scheme            string      `db: "payment_scheme"`
  Payment_type              string      `db: "payment_type"`
  Processing_date           strfmt.Date `db: "processing_date"`
  Reference                 string      `db: "reference"`
  Scheme_payment_sub_type   string      `db: "scheme_payment_sub_type"`
  Scheme_payment_type       string      `db: "scheme_payment_type"`
  Sponsor_id                strfmt.UUID `db: "sponsor_id"`
}

type DBSponsor struct {
  ID             strfmt.UUID `db: "id"`
  Account_number string      `db: "account_number"`
  Bank_id        string      `db: "bank_id"`
  Bank_id_code   string      `db: "bank_id_code"`
}

type DBDebtorParty struct {
  DBSponsor

  Account_name        string `db: "account_name"`
  Account_number_code string `db: "account_number_code"`
  Address             string `db: "address"`
  Name                string `db: "name"`
}

type DBBeneficiaryParty struct {
  DBDebtorParty

  Account_type int64 `db: "account_type"`
}

type DBSenderCharge struct {
  ID         strfmt.UUID `db: "id"`
  Payment_id strfmt.UUID `db: "payment_id"`
  Amount     string      `db: "amount"`
  Currency   string      `db: "currency"`
}
