// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/swag"
)

// DebtorParty debtor party
// swagger:model debtor_party
type DebtorParty struct {
	SponsorParty

	// account name
	AccountName string `json:"account_name,omitempty"`

	// account number code
	AccountNumberCode string `json:"account_number_code,omitempty"`

	// address
	Address string `json:"address,omitempty"`

	// name
	Name string `json:"name,omitempty"`
}

// UnmarshalJSON unmarshals this object from a JSON structure
func (m *DebtorParty) UnmarshalJSON(raw []byte) error {
	// AO0
	var aO0 SponsorParty
	if err := swag.ReadJSON(raw, &aO0); err != nil {
		return err
	}
	m.SponsorParty = aO0

	// AO1
	var dataAO1 struct {
		AccountName string `json:"account_name,omitempty"`

		AccountNumberCode string `json:"account_number_code,omitempty"`

		Address string `json:"address,omitempty"`

		Name string `json:"name,omitempty"`
	}
	if err := swag.ReadJSON(raw, &dataAO1); err != nil {
		return err
	}

	m.AccountName = dataAO1.AccountName

	m.AccountNumberCode = dataAO1.AccountNumberCode

	m.Address = dataAO1.Address

	m.Name = dataAO1.Name

	return nil
}

// MarshalJSON marshals this object to a JSON structure
func (m DebtorParty) MarshalJSON() ([]byte, error) {
	_parts := make([][]byte, 0, 2)

	aO0, err := swag.WriteJSON(m.SponsorParty)
	if err != nil {
		return nil, err
	}
	_parts = append(_parts, aO0)

	var dataAO1 struct {
		AccountName string `json:"account_name,omitempty"`

		AccountNumberCode string `json:"account_number_code,omitempty"`

		Address string `json:"address,omitempty"`

		Name string `json:"name,omitempty"`
	}

	dataAO1.AccountName = m.AccountName

	dataAO1.AccountNumberCode = m.AccountNumberCode

	dataAO1.Address = m.Address

	dataAO1.Name = m.Name

	jsonDataAO1, errAO1 := swag.WriteJSON(dataAO1)
	if errAO1 != nil {
		return nil, errAO1
	}
	_parts = append(_parts, jsonDataAO1)

	return swag.ConcatJSON(_parts...), nil
}

// Validate validates this debtor party
func (m *DebtorParty) Validate(formats strfmt.Registry) error {
	var res []error

	// validation for a type composition with SponsorParty
	if err := m.SponsorParty.Validate(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

// MarshalBinary interface implementation
func (m *DebtorParty) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *DebtorParty) UnmarshalBinary(b []byte) error {
	var res DebtorParty
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
