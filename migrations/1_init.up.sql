CREATE TABLE payment_party (
  id UUID NOT NULL PRIMARY KEY,
  account_name VARCHAR(128),
  account_number VARCHAR(128),
  account_number_code VARCHAR(5),
  account_type INTEGER,
  address VARCHAR(256),
  bank_id VARCHAR(32),
  bank_id_code VARCHAR(5),
  name VARCHAR(64)
);

CREATE TABLE payment (
  id UUID NOT NULL PRIMARY KEY,
  organisation_id VARCHAR(36),
  type VARCHAR(32),
  version INTEGER,
  amount VARCHAR(32),
  beneficiary_party_id UUID REFERENCES payment_party(id),
  bearer_code VARCHAR(5),
  receiver_charges_amount VARCHAR(32),
  receiver_charges_currency VARCHAR(5),
  currency VARCHAR(5),
  debtor_party_id UUID REFERENCES payment_party(id),
  end_to_end_reference VARCHAR(128),
  contract_reference VARCHAR(5),
  exchange_rate VARCHAR(32),
  original_amount VARCHAR(32),
  original_currency VARCHAR(5),
  numeric_reference VARCHAR(16),
  payment_id VARCHAR(64),
  payment_purpose VARCHAR(64),
  payment_scheme VARCHAR(5),
  payment_type VARCHAR(32),
  processing_date DATE,
  reference VARCHAR(128),
  scheme_payment_sub_type VARCHAR(32),
  scheme_payment_type VARCHAR(32),
  sponsor_id UUID REFERENCES payment_party(id)
);

CREATE TABLE sender_charge (
  id UUID NOT NULL PRIMARY KEY,
  payment_id UUID REFERENCES payment(id),
  amount VARCHAR(32),
  currency VARCHAR(5)
);
